# README #

This is the README for the EPOOLICE main content extractor for the SAS crawler. The format is markdown with atx syntax. 

The program will process the raw crawler files. For a file this processing consists of:

* Extracting the document URI from the xml
* Fetching the html from the web
* Extracting the main content of the html
* Writing the main content in the body element of the xml and writing it back to disk

## Usage ##

The program needs two things to run:

* Knowledge of where to find the SAS raw crawler files. This path can be hardcoded in the Main method of the program
* Knowledge of which files it has already processed, so it doesn't process the same files twice. It does this by creating a file that contains the creation time of the latest document it processed last time it ran, and then only processing files that are newer than this. The first time the program runs there is no such file, so the program will create a file containing the current time. This means that by default no files will be processed which have a creation timestamp older than the time when the program is first run. In order to process these files, it is possible to manually edit the timestamp file. The path of this file can be seen in the source code.
