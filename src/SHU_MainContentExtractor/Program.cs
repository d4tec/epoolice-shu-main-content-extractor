﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace SHU_MainContentExtractor
{
    class Program
    {
        static string LATEST_PROCESSED_FILE_CREATION_TIME_FILE = "LatestProcessedFileCreationTime.txt";
        static IFormatProvider ENGLISH_FORMAT = CultureInfo.GetCultureInfo("en");

        static void Main(string[] args)
        {
            EnsureLatestProcessedFileCreationTime();
            string rawCrawlerFilesDirectory = Directory.GetCurrentDirectory(); // change this to wherever the raw crawler files are
            RunBatchMainContentExtraction(rawCrawlerFilesDirectory);
            Console.ReadLine();
        }

        private static void RunBatchMainContentExtraction(string rawCrawlerFilesDirectory)
        {
            DateTime latestProcessedFileCreationTime = GetLatestProcessedFileCreationTime();
            var unprocessedFilesInfos = Directory.EnumerateFiles(rawCrawlerFilesDirectory, "*.xml", SearchOption.TopDirectoryOnly)
                                                 .Select(s => new FileInfo(s))
                                                 .Where(fi => fi.CreationTimeUtc > latestProcessedFileCreationTime)
                                                 .OrderBy(fi => fi.CreationTimeUtc)
                                                 .ToArray();

            int unprocessedFiles = 0;
            int processedFiles = 0;
            Console.WriteLine("Found " + unprocessedFilesInfos.Length + " files, now processing...");
            foreach (var unprocessedFileInfo in unprocessedFilesInfos)
            {
                try
                {
                    PutMainContentInFile(unprocessedFileInfo);
                    MarkLatestProcessedFileCreationTime(unprocessedFileInfo.CreationTimeUtc);
                    processedFiles++;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    unprocessedFiles++;
                }
            }
            Console.WriteLine("Correctly processed " + processedFiles + " files, couldn't process " + unprocessedFiles + " files");
        }

        private static void PutMainContentInFile(FileInfo fileInfo)
        {
            XDocument doc = null;
            var processed = false;
            using (var fileStream = File.OpenRead(fileInfo.FullName))
            {
                doc = XDocument.Load(fileStream);
                var article = doc.Element("article");
                var uri = GetUri(article);
                var html = GetHtml(uri);
                var mainContent = ExtractMainContent(html);
                article.Element("body").Value = mainContent;
                processed = true;
            }
            if (processed)
            {
                doc.Save(fileInfo.FullName);
            }
        }

        // Documents from the rss folder contain two URI elements: 
        // 1: a docid element, which contains the URI of the document itself
        // 2: a sourceurl element, which contains the URI of the rss document that linked to the document
        // Documents from the web folder only have a single URI element:
        // 1: a sourceurl element, which contains the URI of the document itself
        private static Uri GetUri(XElement article)
        {
            XElement docUriElement = null;
            switch (article.Element("sourcetype").Value)
            {
                case "RSS": 
                    docUriElement = article.Element("docid"); 
                    break;
                case "Web":
                    docUriElement = article.Element("sourceurl");
                    break;
                default: 
                    throw new Exception("Unknown document source type");
            }

            var uriDecoded = HttpUtility.HtmlDecode(docUriElement.Value);
            return new Uri(uriDecoded);
        }

        private static string GetHtml(Uri uri)
        {
            var client = new HttpClient();
            client.Timeout = TimeSpan.FromSeconds(5);
            using (var task = client.GetStringAsync(uri))
            {
                task.Wait();
                if (task.IsCompleted)
                {
                    return task.Result;
                }
                else
                {
                    if (task.Exception != null)
                        throw task.Exception;
                    else
                        throw new Exception("Could not fetch url: " + uri);
                }
            }
        }

        private static string ExtractMainContent(string html)
        {
            HttpWebRequest request = MakeMainContentExtractionWebRequest(html);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                using (var reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                {
                    return reader.ReadToEnd();
                }
            }
            else
            {
                throw new Exception("bad main content extraction response: " + response.StatusCode);
            }
        }
            
        private static HttpWebRequest MakeMainContentExtractionWebRequest(string html)
        {
            var uri = new Uri("http://d4nlp-01.cloudapp.net/maincontent/extract");
            var encodedText = "html=" + HttpUtility.UrlEncode(html);
            var bytes = Encoding.ASCII.GetBytes(encodedText);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = bytes.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(bytes, 0, bytes.Length);
            }
            return request;
        }

        private static void EnsureLatestProcessedFileCreationTime()
        {
            // if we can't see a latest processed file creation time file, just mark the time now, so we don't risk processing the same files twice.
            if (!HasLatestProcessedFileCreationTime())
            {
                MarkLatestProcessedFileCreationTime(DateTime.Now);
            }
        }

        private static bool HasLatestProcessedFileCreationTime()
        {
            return File.Exists(LATEST_PROCESSED_FILE_CREATION_TIME_FILE);
        }

        // assumes HasLatestProcessedFileCreationTime()
        private static DateTime GetLatestProcessedFileCreationTime()
        {
            return DateTime.Parse(File.ReadAllText(LATEST_PROCESSED_FILE_CREATION_TIME_FILE), ENGLISH_FORMAT, DateTimeStyles.AdjustToUniversal);
        }

        private static void MarkLatestProcessedFileCreationTime(DateTime fileTime)
        {
            File.WriteAllText(LATEST_PROCESSED_FILE_CREATION_TIME_FILE, fileTime.ToUniversalTime().ToString(ENGLISH_FORMAT));
        }
    }
}
